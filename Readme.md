# HA environment for JMeter+ELK 
## Prerequisites:
Windows 10 with at least 20 Gb of free disk space and 8 Gb of RAM on board
Vagrant 2.2.7
VirtualBox 6.1.0
## How to use
* vagrant up  - to bring up environment

* vagrant ssh swarm-master-1 - to switch on worker machine machine  

* docker stack deploy -c docker-stack.yml elk - to install ELK on swarm-cluster

* jmeter -n -t FindByCity.jmx -l/data/EpamCareerFindCity/results.csv - to run Jmeter test 